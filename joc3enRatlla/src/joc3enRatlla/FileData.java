package joc3enRatlla;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileData implements FileManager{
	DataInputStream in;
	String p1Name;
	String p2Name="";
	long time=0;
	int nMoviments=0;
	public void llegeixPartida(File fitxer) {
		try {
			in = new DataInputStream(new BufferedInputStream(new FileInputStream(fitxer)));
			getCapInfo();
			getMoviments();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getCapInfo() {
		try {
			p1Name=in.readUTF();
			p2Name=in.readUTF();
			time=in.readLong();
			nMoviments=in.readInt();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getMoviments() {
		int i=0;
		int posP1=0;
		int posP2=0;
		try {
			while(i<nMoviments) {
				String player=in.readUTF();
				int nJugada=in.readInt();
				int fila=in.readInt();
				int columna=in.readInt();
				if(player.equals(p1Name)) {
					movimentsP1[posP1][0]=fila;
					movimentsP1[posP1][1]=columna;
					System.out.println("Moviment " + (i+1) + ": "+movimentsP1[posP1][0] +" "+ movimentsP1[posP1][1]);
					posP1++;
				}
				else if(player.equals(p2Name)) {
					movimentsP2[posP2][0]=fila;
					movimentsP2[posP2][1]=columna;
					System.out.println("Moviment " + (i+1) + ": "+movimentsP2[posP2][0] +" "+ movimentsP2[posP2][1]);
					posP2++;
				}
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void escriuPartida(String nPartida, int nMoviments, int[][] moviments) {
		for(int j=0;j<nMoviments;j++) {
			System.out.println(moviments[j][0]+" "+moviments[j][1]);
		}
		try {
			DataOutputStream dOut = new DataOutputStream(new FileOutputStream(nPartida));
			dOut.writeUTF("Jugador 1");
			dOut.writeUTF("Jugador 2");
			dOut.writeDouble(System.currentTimeMillis());
			dOut.writeInt(nMoviments);
			System.out.println("He guardat: ");
			for(int i=0;i<nMoviments;i++) {
				if(i%2==0) {
					dOut.writeUTF("Jugador 1");
					System.out.println("Jugador 1");
				}
				else {
					dOut.writeUTF("Jugador 2");
					System.out.println("Jugador 2");
				}
				dOut.writeInt(i+1);
				dOut.writeInt(moviments[i][0]);
				dOut.writeInt(moviments[i][1]);
				System.out.println(i+1);
				System.out.println(moviments[i][0] + " " + moviments[i][1]);
			}
			dOut.close();
			System.out.println("Partida guardada");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
