package joc3enRatlla;

import java.io.Serializable;

public class Partida implements Serializable{
	String p1;
	String p2;
	int[][] moviments = new int[9][2];
	int nMoviments;
	long time;
	
	Partida(String p1, String p2, int[][] moviments, int nMoviments, long time) {
		this.p1=p1;
		this.p2=p2;
		this.moviments=moviments;
		this.nMoviments=nMoviments;
		this.time=time;
	}

	public String getP1() {
		return p1;
	}

	public void setP1(String p1) {
		this.p1 = p1;
	}

	public String getP2() {
		return p2;
	}

	public void setP2(String p2) {
		this.p2 = p2;
	}

	public int[][] getMoviments() {
		return moviments;
	}

	public void setMoviments(int[][] moviments) {
		this.moviments = moviments;
	}

	public int getnMoviments() {
		return nMoviments;
	}

	public void setnMoviments(int nMoviments) {
		this.nMoviments = nMoviments;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
}
