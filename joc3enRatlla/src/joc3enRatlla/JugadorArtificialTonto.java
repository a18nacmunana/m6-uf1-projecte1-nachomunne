package joc3enRatlla;

public class JugadorArtificialTonto extends Jugador{
	JugadorArtificialTonto(String caracter){
		this.caracter=caracter;
	}
	public String getFicha(){
		return caracter;
	}
	
	public void jugada(String [][] tauler, int[][] moviments, int nMoviments) {
		boolean comprova=false;
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++) {
				if(tauler[i][j]==" ") {
						tauler[i][j]=caracter;
						comprova=true;
						break;
				}
			}
			if(comprova==true)
				break;
		}
	}
}
