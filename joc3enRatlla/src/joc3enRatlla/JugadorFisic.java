package joc3enRatlla;
import java.util.Scanner;

public class JugadorFisic extends Jugador{
	Scanner sca = new Scanner(System.in);
	JugadorFisic(String caracter){
		this.caracter=caracter;
	}

	public String getFicha(){
		return caracter;
	}
	
	public void jugada(String [][] tauler, int[][] moviments, int nMoviments){
		boolean ocupat=true;
		int fila;
		int columna;
		do {
			System.out.print("Introdueix la fila: ");
			fila = sca.nextInt();
			System.out.print("Introdueix la columna: ");
			columna = sca.nextInt();
			if (tauler[fila-1][columna-1].equals(" "))
				ocupat=false;
		} while(ocupat==true);
		tauler[fila-1][columna-1]=caracter;
		moviments[nMoviments][0]=fila;
		moviments[nMoviments][1]=columna;
		System.out.println("Moviment " + nMoviments+": "+ moviments[nMoviments][0] +" "+ moviments[nMoviments][1]);
	}
}
