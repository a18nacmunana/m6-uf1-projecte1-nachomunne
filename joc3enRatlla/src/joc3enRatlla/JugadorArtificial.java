package joc3enRatlla;

public class JugadorArtificial extends Jugador{
	JugadorArtificial(String caracter){
		this.caracter=caracter;
	}
	public String getFicha(){
		return caracter;
	}
	public void jugada(String [][] tauler, int[][] moviments, int nMoviments) {
		for(int i=0;i<3;i++){
			if(tauler[i][0]!=" " && (tauler[i][0].equals(tauler[i][1]) || tauler[i][0].equals(tauler[i][2])))
				for(int j=0;j<3;j++) 
					if(tauler[i][j].equals(" ")) {
						tauler[i][j]=caracter;
						moviments[nMoviments][0]=i+1;
						moviments[nMoviments][1]=j+1;
						System.out.println("Moviment " + nMoviments+": "+ moviments[nMoviments][0] +" "+ moviments[nMoviments][1]);
						return;
					}
			else if(tauler[i][1]!=" " && (tauler[i][1].equals(tauler[i][2]))) {
				tauler[i][0]=caracter;
				moviments[nMoviments][0]=i+1;
				moviments[nMoviments][1]=0+1;
				System.out.println("Moviment " + nMoviments+": "+ moviments[nMoviments][0] +" "+ moviments[nMoviments][1]);
				return;
			}	
			else if(tauler[0][i]!=" " && (tauler[0][i].equals(tauler[1][i]) || tauler[0][i].equals(tauler[2][i])))
				for(int k=0;k<3;k++) 
					if(tauler[k][i].equals(" ")){
						tauler[k][i]=caracter;
						moviments[nMoviments][0]=k+1;
						moviments[nMoviments][1]=i+1;
						System.out.println("Moviment " + nMoviments+": "+ moviments[nMoviments][0] +" "+ moviments[nMoviments][1]);
						return;
					}
			else if(tauler[1][i]!=" " && (tauler[1][i].equals(tauler[2][i]))) {
						tauler[0][i]=caracter;
						moviments[nMoviments][0]=0+1;
						moviments[nMoviments][1]=i+1;
						System.out.println("Moviment " + nMoviments+": "+ moviments[nMoviments][0] +" "+ moviments[nMoviments][1]);
						return;
			}	
		}
		
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++) {
				if(tauler[i][j]==" ") {
					tauler[i][j]=caracter;
					moviments[nMoviments][0]=i+1;
					moviments[nMoviments][1]=j+1;
					System.out.println("Moviment " + nMoviments+": "+ moviments[nMoviments][0] +" "+ moviments[nMoviments][1]);
					return;
				}
			}
		}

	}
}