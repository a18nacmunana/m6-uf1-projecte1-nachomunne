package joc3enRatlla;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public interface FileManager {
	int[][] movimentsP1 = new int[5][2];
	int[][]movimentsP2 = new int[5][2];
	
	void llegeixPartida(File fitxer);
	
	void getCapInfo();
	
	void getMoviments();
	
	void escriuPartida(String nPartida, int nMoviments, int[][]moviments);
}
