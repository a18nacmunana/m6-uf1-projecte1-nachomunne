package joc3enRatlla;

import java.io.DataInputStream;
import java.io.IOException;

public class PintadorPartida extends Jugador{
	int[][] moviments = new int[5][2];
	int pos;
	PintadorPartida(String caracter, int[][]moviments){
		this.caracter=caracter;
		this.moviments=moviments;
		pos=0;
	}
	
	public void jugada(String[][] tauler, int[][] bmoviments, int nMoviments) {
		if (tauler[moviments[pos][0]-1][moviments[pos][1]-1].equals(" ")) {
			tauler[moviments[pos][0]-1][moviments[pos][1]-1]=caracter;
			pos++;
		}
		else {
			System.out.println("Partida no funcional");
			System.exit(1);
		}
	}

	public String getFicha() {
		return caracter;
	}

}
