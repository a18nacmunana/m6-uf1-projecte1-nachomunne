package joc3enRatlla;

import java.io.File;

public class prova {

	public static void main(String[] args) {
		System.out.println("-------Comença partida----------");
		JugadorFisic p1 = new JugadorFisic("X");
		JugadorArtificial p2 = new JugadorArtificial("O");
		Jugador[] jugadors=new Jugador[2];
		jugadors[0]=p1;
		jugadors[1]=p2;
		Joc joc = new Joc(jugadors);
		joc.simula();
		
		FileObject fm = new FileObject();
		fm.escriuPartida("provaObject", joc.nMoviments, joc.moviments);
		fm.llegeixPartida(new File("provaObject"));
		
		PintadorPartida pp1 = new PintadorPartida("X", fm.movimentsP1);
		PintadorPartida pp2 = new PintadorPartida("O", fm.movimentsP2);
		jugadors[0]=pp1;
		jugadors[1]=pp2;
		joc.simula();
		
	}

}
