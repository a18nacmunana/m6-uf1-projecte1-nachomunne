package joc3enRatlla;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public class FileText implements FileManager{
	BufferedReader br;
	String p1Name;
	String p2Name;
	String time;
	int nMoviments;
	
	public void llegeixPartida(File fitxer) {
		try {
			br = new BufferedReader(new FileReader (fitxer,StandardCharsets.UTF_16));
			getCapInfo();
			getMoviments();
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getCapInfo() {
		try {
			p1Name=br.readLine();
			p2Name=br.readLine();
			time=br.readLine();
			nMoviments=Integer.parseInt(br.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getMoviments() {
		int posP1=0;
		int posP2=0;
		try {
			for(int i=0;i<nMoviments;i++) {
				String player=br.readLine();
				int nJugada=Integer.parseInt(br.readLine());
				int fila=Integer.parseInt(br.readLine());
				int columna=Integer.parseInt(br.readLine());
				if(player.equals(p1Name)) {
					movimentsP1[posP1][0]=fila;
					movimentsP1[posP1][1]=columna;
					System.out.println("Moviment " + (i+1) + ": "+movimentsP1[posP1][0] +" "+ movimentsP1[posP1][1]);
					posP1++;
				}
				else if(player.equals(p2Name)) {
					movimentsP2[posP2][0]=fila;
					movimentsP2[posP2][1]=columna;
					System.out.println("Moviment " + (i+1) + ": "+movimentsP2[posP2][0] +" "+ movimentsP2[posP2][1]);
					posP2++;
				}
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

	public void escriuPartida(String nPartida, int nMoviments, int[][] moviments) {
		try {
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(nPartida+".txt", StandardCharsets.UTF_16)));
			
			pw.println("Jugador 1");
			pw.println("Jugador 2");
			pw.println(Long.toString(System.currentTimeMillis()));
			pw.println(Integer.toString(nMoviments));
			
			for(int i=0;i<nMoviments;i++) {
				if(i%2==0) {
					pw.println("Jugador 1");
					System.out.println("Jugador 1");
				}
				else {
					pw.println("Jugador 2");
					System.out.println("Jugador 2");
				}
				pw.println(Integer.toString(i+1));
				pw.println(Integer.toString(moviments[i][0]));
				pw.println(Integer.toString(moviments[i][1]));
				System.out.println(i+1);
				System.out.println(moviments[i][0] + " " + moviments[i][1]);
			}
			
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
