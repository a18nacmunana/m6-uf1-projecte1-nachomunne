package joc3enRatlla;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileEnric implements FileManager{
	FileInputStream is;
	int[][] mp1 = new int[5][3];
	int mp1pos=0;
	int[][] mp2 = new int[5][3];
	int mp2pos=0;
	int[] j1= new int[4];
	int[] j2= new int[4];
	int[] data = new int[8];
	int nJugades;
	
	public void llegeixPartida(File fitxer) {
		try {
			is = new FileInputStream(fitxer);
			getCapInfo();
			getMoviments();
			
			for(int i=0; i<mp1.length-1;i++) 
				for(int j=i+1; j<mp1.length;j++) {
					if(mp1[i][2]>mp1[j][2]) {
						int[] tmp= mp1[i];
						mp1[i]=mp1[j];
						mp1[j]=tmp;
					}
				}
			for(int i=0; i<mp2.length-1;i++) 
				for(int j=i+1; j<mp2.length;j++) {
					if(mp2[i][2]>mp2[j][2]) {
						int[] tmp= mp2[i];
						mp2[i]=mp2[j];
						mp2[j]=tmp;
					}
				}

			int skips=0;
			for(int i=0;i<mp1.length;i++) {
				if(mp1[i][2]!=0) {
					movimentsP1[i-skips][0]=mp1[i][0];
					movimentsP1[i-skips][1]=mp1[i][1];
				}
				else
					skips++;
			}
			skips=0;
			for(int i=0;i<mp2.length;i++) {
				if(mp2[i][2]!=0) {
					movimentsP2[i-skips][0]=mp2[i][0];
					movimentsP2[i-skips][1]=mp2[i][1];
				}
				else
					skips++;
			}
			is.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getCapInfo() {		
		try {
			for(int i=0; i<4;i++)
				j1[i]= is.read();
			
			for (int i=0;i<4;i++)
				j2[i]=is.read();
			
			for(int i=0;i<8;i++)
				data[i] = is.read();
			
			nJugades=is.read();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getMoviments() {
		try{
			for(int i=0;i<nJugades; i++) {
				int[] jugador= new int[4];
				int nJugada;
				int fila;
				int columna;
				
				for(int j=0;j<4;j++) 
					jugador[j]=is.read();
				
				nJugada=is.read();
				fila=is.read();
				columna=is.read();
				if(transforma4BytesBigEndianAInt(jugador)==transforma4BytesBigEndianAInt(j1)) {
					mp1[mp1pos][0]=fila+1;
					mp1[mp1pos][1]=columna+1;
					mp1[mp1pos][2]=nJugada;
					mp1pos++;
				}
				else if(transforma4BytesBigEndianAInt(jugador)==transforma4BytesBigEndianAInt(j2)) {
					mp2[mp2pos][0]=fila+1;
					mp2[mp2pos][1]=columna+1;
					mp2[mp2pos][2]=nJugada;
					mp2pos++;
				}
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	private static int transforma4BytesBigEndianAInt(int [] b){
		  int resultat = b[3] +
		    b[2] * 256 +
		    b[1] * 256 * 256 +
		    b[0] * 256 * 256 * 256;
		  return resultat;
	}

	@Override
	public void escriuPartida(String nPartida, int nMoviments, int[][] moviments) {
		// TODO Auto-generated method stub
		
	}

}
