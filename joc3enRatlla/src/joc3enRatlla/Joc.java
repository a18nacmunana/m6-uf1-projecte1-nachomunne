package joc3enRatlla;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Joc {
	String [][] tauler = {{" "," "," "},{" "," "," "},{" "," "," "}};
	public Jugador[] jugadors;
	int nMoviments;
	int moviments[][]= new int[9][2];
	
	public void mostrarResultats(){
		System.out.print("---------\n");
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				System.out.print(tauler[i][j]);
			}
			System.out.print("\n");
		}
		System.out.print("---------\n");
	}
	
	public boolean comprovaVictoria() {
		boolean comprova=false;
		String fichaP1=jugadors[0].getFicha()+jugadors[0].getFicha()+jugadors[0].getFicha();
		String fichaP2=jugadors[1].getFicha()+jugadors[1].getFicha()+jugadors[1].getFicha();
		String fila=tauler[0][0]+tauler[0][1]+tauler[0][2];
		if(fichaP1.equals(fila) || fichaP2.equals(fila))
			comprova=true;
		
		fila=tauler[1][0]+tauler[1][1]+tauler[1][2];
		if(fichaP1.equals(fila) || fichaP2.equals(fila))
			comprova=true;
		
		fila=tauler[2][0]+tauler[2][1]+tauler[2][2];
		if(fichaP1.equals(fila) || fichaP2.equals(fila))
			comprova=true;
		
		String columna=tauler[0][0]+tauler[1][0]+tauler[2][0];
		if(fichaP1.equals(columna) || fichaP2.equals(columna))
			comprova=true;
		
		columna=tauler[0][1]+tauler[1][1]+tauler[2][1];
		if(fichaP1.equals(columna) || fichaP2.equals(columna))
			comprova=true;
		
		columna=tauler[0][2]+tauler[1][2]+tauler[2][2];
		if(fichaP1.equals(columna) || fichaP2.equals(columna))
			comprova=true;
		
		String diagonal=tauler[0][0]+tauler[1][1]+tauler[2][2];
		if(fichaP1.equals(diagonal) || fichaP2.equals(diagonal))
			comprova=true;
		
		diagonal=tauler[0][2]+tauler[1][1]+tauler[2][0];
		if(fichaP1.equals(diagonal) || fichaP2.equals(diagonal))
			comprova=true;
		
		return comprova;
	}
	
	public boolean comprovaEmpat() {
		boolean comprova = true;
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				if(tauler[i][j]==" ")
					comprova=false;
			}
		}
			
		return comprova;
	}
	
	public Joc(Jugador[] jugadors){
		this.jugadors=jugadors;
		this.nMoviments=0;
	}
	
	public void simula(){
		do{
			jugadors[0].jugada(tauler,moviments, nMoviments);
			nMoviments++;
			mostrarResultats();
			if(comprovaVictoria()) {
				System.out.println("Ha guanyat el jugador1");
				break;
			}
			if(comprovaEmpat()){
				System.out.println("Empat");
				return;
			}
			jugadors[1].jugada(tauler,moviments, nMoviments);
			nMoviments++;
			mostrarResultats();
			if(comprovaVictoria()) {
				System.out.println("Ha guanyat el jugador2");
				break;
			}
		}while(comprovaVictoria()==false && comprovaEmpat()==false);
	}

}