package joc3enRatlla;

import java.io.File;
import java.util.Scanner;
/*
 * Petita guia de funcionament
 * 
 * 1ra pregunta: "s" per a carregar una partida "e" per a carregar l'exemple de l'Enric 
 * enter per a jugar una partida (en acabar la partida et pregunta si la vols desar)
 * 
 * Quan desa l'arxiu en .txt inclou l'extensió automàticament, no cal afegir-la al nom
 * 
 * 
 */
public class Principal {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Vols carregar una partida? S/n (per executar l'exemple posa \"E/e\")");
		String resp = scan.nextLine();
		if(resp.equals("S") || resp.equals("s")) {
			System.out.println("Quin fitxer vols obrir? ('partida' és un exemple funcional)");
			String nFitxer=scan.nextLine();
			File fitxer = new File(nFitxer);
			PintadorPartida p1;
			PintadorPartida p2;
			if(nFitxer.endsWith(".txt")) {
				FileText fm = new FileText();
				fm.llegeixPartida(fitxer);
				p1 = new PintadorPartida("X", fm.movimentsP1);
				p2 = new PintadorPartida("O", fm.movimentsP2);
			}
			else {
				FileData fileManager = new FileData();
				fileManager.llegeixPartida(fitxer);
				p1 = new PintadorPartida("X", fileManager.movimentsP1);
				p2 = new PintadorPartida("O", fileManager.movimentsP2);
			}
			Jugador[] jugadors=new Jugador[2];
			jugadors[0]=p1;
			jugadors[1]=p2;
			Joc joc = new Joc(jugadors);
			joc.simula();
			
		}
		else if(resp.equals("E") || resp.equals("e")) {
			File fitxer = new File("partida-v1.tres");
			
			FileEnric fileManager = new FileEnric();
			fileManager.llegeixPartida(fitxer);
			PintadorPartida p1 = new PintadorPartida("X", fileManager.movimentsP1);
			PintadorPartida p2 = new PintadorPartida("O", fileManager.movimentsP2);
			
			Jugador[] jugadors=new Jugador[2];
			jugadors[0]=p1;
			jugadors[1]=p2;
			Joc joc = new Joc(jugadors);
			joc.simula();
		}
		
		else {
			System.out.println("-------Comença partida----------");
			JugadorFisic p1 = new JugadorFisic("X");
			JugadorArtificial p2 = new JugadorArtificial("O");
			Jugador[] jugadors=new Jugador[2];
			jugadors[0]=p1;
			jugadors[1]=p2;
			Joc joc = new Joc(jugadors);
			joc.simula();
			
			System.out.println("Vols guardar la partida? S/n");
			resp=scan.nextLine();
			if(resp.equals("s") || resp.equals("S")) {
				System.out.println("Quin nom li vols donar a l'arxiu? ");
				String nFitxer=scan.nextLine();
				
				System.out.println("1. TXT \n2. DataInput \n(per defecte txt)");
				int format= scan.nextInt();
				switch(format) {
				  case 2:
					FileData fm = new FileData();
					fm.escriuPartida(nFitxer, joc.nMoviments, joc.moviments);
				    break;
				    
				  default:
					FileText fileManager = new FileText();
					fileManager.escriuPartida(nFitxer, joc.nMoviments, joc.moviments);
				}
			}
			else 
				System.out.println("El programa ha finalitzat");
		}
	}
}
