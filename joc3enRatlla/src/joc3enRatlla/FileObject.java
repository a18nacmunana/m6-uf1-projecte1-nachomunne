package joc3enRatlla;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileObject implements FileManager{
	ObjectInputStream oi;
	Partida partida;
	
	
	public void llegeixPartida(File fitxer) {
		try {
			oi = new ObjectInputStream(new FileInputStream(fitxer));
			partida = (Partida) oi.readObject();
			System.out.println(partida.p1);
			getCapInfo();
			getMoviments();
			oi.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void getCapInfo() {
		//Com que no opero amb aquestes dades, i si volgués ja les tinc al objecte, no fa falta que les torni a desar
	}

	public void getMoviments() {
		int posP1=0;
		int posP2=0;
		for(int i= 0; i<partida.moviments.length;i++) {
			if(i%2==0) {
				movimentsP1[posP1]=partida.moviments[i];
				posP1++;
			}
			else {
				movimentsP2[posP2]=partida.moviments[i];
				posP2++;
			}
		}
		for(int i=0; i<movimentsP2.length;i++)
			System.out.println((i+1)+". "+ movimentsP1[i][0] +" " + movimentsP1[i][1]);
	}

	public void escriuPartida(String nPartida, int nMoviments, int[][] moviments) {
		try {
			ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream(nPartida));
			Partida partida = new Partida("Jugador 1", "Jugador 2", moviments, nMoviments, System.currentTimeMillis());
			oout.writeObject(partida);
			oout.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
